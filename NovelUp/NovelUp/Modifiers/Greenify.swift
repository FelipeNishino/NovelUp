//
//  GreenButton.swift
//  NovelUp
//
//  Created by Felipe Nishino on 26/11/20.
//

import SwiftUI

struct Greenify : ViewModifier {
    func body(content: Content) -> some View {
        content
            .buttonStyle(PlainButtonStyle())
            .foregroundColor(.white)
            .background(Color("Background"))
            .cornerRadius(5)
    }
    
    private let cardCornerRadius : CGFloat = 15
    private let edgeLineWidth : CGFloat = 3
}

extension View {
    /// Modificador para aplicar a identidade visual dos botões (Ex. Próximo)
    func greenify() -> some View {
        modifier(Greenify())
    }
}
