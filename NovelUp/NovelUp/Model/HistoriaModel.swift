//
//  HistoriaModel.swift
//  NovelUp
//
//  Created by Rodrigo Carvalho on 11/11/20.
//

import SwiftUI

extension History {
    
    func updateImage(imagem: UIImage?) {
        if imagem == nil {
            return
        } else {
        let imageData = imagem!.pngData()
        self.image = imageData
        }
    }
    
    func updateTitle(title: String) {
        if title == ""{
            return
        } else {
            self.title = title
        }
    }
    
    func updateSinopse(sinopse: String) {
        if sinopse == ""{
            return
        } else {
            self.synopsis = sinopse
        }
    }
    
    func updateTags(tags: String) {
        if tags == ""{
            return
        } else {
            self.tags = tags
        }
    }
    
    func updateGenre(genero: String) {
        if genero == ""{
            return
        }else {
            self.genre = genero
        }
    }
}
