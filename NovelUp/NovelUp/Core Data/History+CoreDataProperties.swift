//
//  History+CoreDataProperties.swift
//  NovelUp
//
//  Created by Rodrigo Carvalho on 29/10/20.
//
//

import Foundation
import CoreData


extension History {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<History> {
        return NSFetchRequest<History>(entityName: "History")
    }

    @NSManaged public var creationDate: Date?
    @NSManaged public var id: UUID?
    @NSManaged public var lastViewedPage: Int16
    @NSManaged public var title: String?
    @NSManaged public var progress: String
    @NSManaged public var pages: NSSet?
    @NSManaged public var synopsis: String?
    @NSManaged public var genre: String?
    @NSManaged public var tags: String?
    @NSManaged public var image: Data?
    
    var historyProgress: Progress {
        set {
            progress = newValue.rawValue
        }
        get {
            Progress (rawValue: progress) ?? .phaseOne
        }
    }
}

// MARK: Generated accessors for pages
extension History {

    @objc(addPagesObject:)
    @NSManaged public func addToPages(_ value: Page)

    @objc(removePagesObject:)
    @NSManaged public func removeFromPages(_ value: Page)

    @objc(addPages:)
    @NSManaged public func addToPages(_ values: NSSet)

    @objc(removePages:)
    @NSManaged public func removeFromPages(_ values: NSSet)

}

extension History : Identifiable {

}

enum Progress: String {
    case phaseOne = "Fase 1"
    case phaseTwo = "Fase 2"
    case phaseThree = "Fase 3"
}
