//
//  Fases.swift
//  NovelUp
//
//  Created by Rodrigo Carvalho on 11/11/20.
//

import Foundation

struct Fases {
    enum Info : Int {
        case descricao
        case dica
    }
    
    static let explicacaoFase = [
    /// To access informations for each cell, access the dictionary with keys as follows: (Phase.id) + (Cell.id)
        NSLocalizedString("Explicação1", comment: ""),
        NSLocalizedString("Explicação2", comment: ""),
        NSLocalizedString("Explicação3", comment: "")
    ]
    
    static let descricaoDica = [
        //Fase 1
        "11" : [ NSLocalizedString("Era uma vez…",comment: "") , NSLocalizedString("Introduza seu personagem e cenário. Explique sobre quem e onde a história se passa.", comment: "") ],
        "12" : [ NSLocalizedString("E todo dia…", comment: "") , NSLocalizedString("Conte como é a rotina do seu personagem", comment: "") ],
        "13" : [ NSLocalizedString("Até que um dia…", comment: "") , NSLocalizedString("Crie um acontecimento que quebre a rotina estabelecida.", comment: "") ],
        "14" : [ NSLocalizedString("E por causa disso…", comment: "") , NSLocalizedString("Quais as consequências que esse acontecimento trouxe?", comment: "") ],
        "15" : [ NSLocalizedString("E por causa disso…", comment: "") , NSLocalizedString("O que seu personagem deve fazer agora?",comment: "") ],
        "16" : [ NSLocalizedString("E por causa disso…", comment: "") , NSLocalizedString("O objetivo foi concluído, mas e agora?", comment: "") ],
        "17" : [ NSLocalizedString("Até que finalmente…", comment: "") , NSLocalizedString("Este é o clímax da sua história, seu personagem será bem-sucedido ou falhará?", comment: "") ],
        "18" : [ NSLocalizedString("E deste dia em diante…", comment: "") , NSLocalizedString("Uma nova rotina é estabelecida. Que tal dizer qual foi a moral da história?", comment: "") ],
        //Fase 2
        "21" : [ NSLocalizedString("Introdução", comment: "") , NSLocalizedString("Diga ao leitor sobre o mundo em que a história se passa.", comment: "") ],
        "22" : [ NSLocalizedString("Defina a cena", comment: "") , NSLocalizedString("Mostre qual a rotina desse mundo. Não esqueça de descrever a ambientação.", comment: "") ],
        "23" : [ NSLocalizedString("Introduza seu personagem", comment: "") , NSLocalizedString("Além de características físicas, fale também sobre sua personalidade, hábitos e hobbies.", comment: "") ],
        "24" : [ NSLocalizedString("Uma série de eventos", comment: "") , NSLocalizedString("Algo deve acontecer para tirar seu personagem da rotina.", comment: "") ],
        "25" : [ NSLocalizedString("Conflitos e obstáculos", comment: "") , NSLocalizedString("Não se prenda a obstáculos físicos, trabalhe com o psicológico também. Talvez algum trauma do passado?", comment: "") ],
        "26" : [ NSLocalizedString("Reviravolta", comment: "") , NSLocalizedString("Aqui seu personagem pode superar os obstáculos (sozinho ou com ajuda) ou falhar.", comment: "") ],
        "27" : [ NSLocalizedString("Mudança no personagem", comment: "") , NSLocalizedString("Como essa reviravolta afetou seu personagem? Talvez ele tenha se tornado mais confiante, ou não…", comment: "") ],
        "28" : [ NSLocalizedString("Clímax",comment: "") , NSLocalizedString("Esse é o momento mais importante na jornada do seu personagem. O acontecimento que irá definir o final da sua história.", comment: "") ],
        "29" : [ NSLocalizedString("Resolução", comment: "") ,  NSLocalizedString("Não conte apenas o final do seu personagem, mas como essa jornada afetou o mundo da sua história.", comment: "") ],
        //Fase 3
        "31" : [ NSLocalizedString("O mundo",comment: "") , NSLocalizedString("Introduza onde sua história se passa. Como ele é, como funciona, em que época estamos?", comment: "") ],
        "32" : [ NSLocalizedString("O personagem", comment: "") , NSLocalizedString("Além da aparência e personalidade, mostre seu dia a dia, como se sente. Talvez algum dilema que ele enfrente.", comment: "") ],
        "33" : [ NSLocalizedString("A mudança/oportunidade",comment: "") , NSLocalizedString("Algo fora do comum deve acontecer. Não precisa ser necessariamente algo ruim.", comment: "") ],
        "34" : [ NSLocalizedString("O momento de hesitação",comment: "") , NSLocalizedString("Algo impede seu personagem de seguir em frente. Sua família? Seus amigos?", comment: "") ],
        "35" : [ NSLocalizedString("A decisão", comment: "") , NSLocalizedString("Agora é a hora de tomar atitude para enfrentar a situação fora do comum que você criou antes.", comment: "") ],
        "36" : [ NSLocalizedString("Os conflitos e obstáculos", comment: "") , NSLocalizedString("Nem tudo acontece como queremos. Algo inesperado que dificulte o trajeto do seu personagem. Um inimigo?", comment: "") ],
        "37" : [ NSLocalizedString("O apoio", comment: "") , NSLocalizedString("Um amigo ou um mentor que ajude o seu personagem nessa jornada. Ele não precisa aparecer na cena, talvez alguma lembrança com eles seja o suficiente.", comment: "") ],
        "38" : [ NSLocalizedString("A provação", comment: "") , NSLocalizedString("O momento que irá definir viver ou morrer. Hora de enfrentar os piores medos do seu personagem.", comment: "")],
        "39" : [ NSLocalizedString("O resultado", comment: "") , NSLocalizedString("Como a provação mudou seu personagem? Ele conseguiu o que queria ou falhou?", comment: "") ],
        "310" : [ NSLocalizedString("O clímax", comment: "") , NSLocalizedString("Quando tudo parece decidido, vem uma reviravolta. O momento em que seu personagem está prestes a perder o que conquistou até agora.", comment: "") ],
        "311" : [ NSLocalizedString("O final da jornada",comment: "") , NSLocalizedString("O que foi conquistado ao final? Como seu personagem está agora?", comment: "") ],
        "312" : [ NSLocalizedString("As consequências",  comment: "") , NSLocalizedString("Aproveite para fechar todos os pontos da história que você deixou em aberto. Mostre como essa jornada afetou o mundo em volta do personagem.", comment: "") ],
        ]
}
