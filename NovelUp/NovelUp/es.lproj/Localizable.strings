/* 
  Localizable.strings
  NovelUp

  Created by Rodrigo Carvalho on 25/11/20.
  
*/
"Histórias"           = "Cuentos";
"História"            = "Cuento";
"Criar nova história" = "Crear nueva historia";
"Nº Palavras: "       = "Nº de Palabras: ";
"Data de criação: "   = "Fecha de creación: ";
"Sinopse:"            = "Sinopsis:";
"Gênero:"             = "Género:";
"A sinopse ainda não foi escrita" = "La sinopsis aún no se ha escrito";
"Gênero ainda não definido" = "Género aún no definido";
"Nenhuma tag definida" = "No hay tag definidas";
"Fase "                = "Fase ";
"Escrever História"    = "Escribir Cuento";
"Não mostrar novamente" = "No volver a mostrar";
"Cancelar"            = "Cancelar";
"Título:"             = "Título:";
"Tags:"               = "Tags:";
"Alterar Imagem"      = "Cambiar Imagen";
"Salvar Alterações"   = "Guardar Ediciones";
"Buscar suas histórias" = "Buscar sus historias";
"Ação"                = "Acción";
"Aventura"            = "Aventura";
"Comédia"             = "Comedia";
"Drama"               = "Drama";
"Fantasia"            = "Fantasía";
"Ficção"              = "Ficción";
"Ficção Científica"   = "Ficción Científica";
"Mistério"            = "Misterio";
"Poesia"              = "Poesía";
"Policial"            = "Policial";
"Romance"             = "Romance";
"Suspense"            = "Thriller";
"Terror"              = "Horror";
"Tragédia"            = "Tragedia";
"Salvar"              = "Guardar";

"Anterior" = "Anterior";
"Próxima" = "Siguiente";
"Finalizado" = "Terminado";
"Finalizar" = "Terminar";
"Sair" = "Salir";
// -------------FASE 1-----------//


"Era uma vez…" = "Era una vez…";
"Introduza seu personagem e cenário. Explique sobre quem e onde a história se passa." = "Presenta su personaje y escenario. Explique quién y dónde se desarrolla la historia.";

"E todo dia…" = "Y cada día…";
"Conte como é a rotina do seu personagem" = "Cuenta sobre la rutina de tu personaje";

"Até que um dia…" = "Hasta que un día…";
"Crie um acontecimento que quebre a rotina estabelecida." = "Crea un evento que rompa la rutina establecida.";

"E por causa disso…" = "Y debido a eso…";
"Quais as consequências que esse acontecimento trouxe?" = "¿Cuáles son las consecuencias que trajo este evento?";
"O que seu personagem deve fazer agora?" = "¿Qué debería hacer tu personaje ahora?";
"O objetivo foi concluído, mas e agora?" = "El objetivo se ha cumplido. ¿Qué hacer ahora?";

"Até que finalmente…" = "Hasta que finalmente…";
"Este é o clímax da sua história, seu personagem será bem-sucedido ou falhará?" = "Este es el clímax de tu historia. ¿Tu personaje tendrá éxito o fracasará?";

"E deste dia em diante…" = "Y a partir de este día…";
"Uma nova rotina é estabelecida. Que tal dizer qual foi a moral da história?" = "Se establece una nueva rutina. ¿Qué tal decir la moraleja de la historia?";


// -------------FASE 2-----------//


"Introdução" = "Introducción";
"Diga ao leitor sobre o mundo em que a história se passa." = "Cuéntele al lector sobre el mundo en el que se desarrolla la historia.";

"Defina a cena" = "Establece la escena";
"Mostre qual a rotina desse mundo. Não esqueça de descrever a ambientação." = "Muestra la rutina de este mundo. No se olvide de describir el escenario.";

"Introduza seu personagem" = "Presenta tu personaje";
"Além de características físicas, fale também sobre sua personalidade, hábitos e hobbies." = "Además de las características físicas, también hable sobre su personalidad, hábitos y pasatiempos.";

"Uma série de eventos" = "Una serie de eventos";
"Algo deve acontecer para tirar seu personagem da rotina." = "Algo debe suceder para sacar a tu personaje de la rutina.";

"Conflitos e obstáculos" = "Conflictos y obstáculos";
"Não se prenda a obstáculos físicos, trabalhe com o psicológico também. Talvez algum trauma do passado?" = "No te limites a los obstáculos físicos, trabaja también con los psicológicos. ¿Quizás algún trauma del pasado?";

"Reviravolta" = "Cambio";
"Aqui seu personagem pode superar os obstáculos (sozinho ou com ajuda) ou falhar." = "Aquí tu personaje puede superar obstáculos (solo o con ayuda) o fallar.";

"Mudança no personagem" = "Cambio de carácter";
"Como essa reviravolta afetou seu personagem? Talvez ele tenha se tornado mais confiante, ou não…" = "¿Cómo afectó el último cambio a tu personaje? Tal vez se ha vuelto más seguro, o no…";

"Clímax" = "Clímax";
"Esse é o momento mais importante na jornada do seu personagem. O acontecimento que irá definir o final da sua história." = "Este es el momento más importante en el viaje de tu personaje. El evento que definirá el final de tu historia.";

"Resolução" = "Resolución";
"Não conte apenas o final do seu personagem, mas como essa jornada afetou o mundo da sua história." = "No solo cuentes el final de tu personaje, sino cómo ese viaje ha afectado al mundo de tu historia.";


// -------------FASE 3-----------//


"O mundo" = "El mundo";
"Introduza onde sua história se passa. Como ele é, como funciona, em que época estamos?" = "Presenta el lugar donde se desarrolla su historia. ¿Cómo es, cómo funciona, en qué tiempo estamos?";

"O personagem" = "El personaje";
"Além da aparência e personalidade, mostre seu dia a dia, como se sente. Talvez algum dilema que ele enfrente." = "Además de la apariencia y la personalidad, muestre su vida diaria, cómo se siente. Quizás algún dilema al que se enfrenta.";

"A mudança/oportunidade" = "El cambio/oportunidad";
"Algo fora do comum deve acontecer. Não precisa ser necessariamente algo ruim." = "Algo fuera de lo común debe suceder. No necesariamente tiene que ser algo malo.";

"O momento de hesitação" = "El momento de vacilación";
"Algo impede seu personagem de seguir em frente. Sua família? Seus amigos?" = "Algo impide que tu personaje avance. ¿Tu familia? ¿Sus amigos?";

"A decisão" = "La decisión";
"Agora é a hora de tomar atitude para enfrentar a situação fora do comum que você criou antes." = "Ahora es el momento de tomar medidas para enfrentar la situación inusual que creó antes.";

"Os conflitos e obstáculos" = "Los conflictos y obstáculos";
"Nem tudo acontece como queremos. Algo inesperado que dificulte o trajeto do seu personagem. Um inimigo?" = "No todo sucede como queremos. Algo inesperado que dificultará el camino de tu personaje. ¿Un enemigo?";

"O apoio" = "El apoyo";
"Um amigo ou um mentor que ajude o seu personagem nessa jornada. Ele não precisa aparecer na cena, talvez alguma lembrança com eles seja o suficiente." = "Un amigo o mentor para ayudar a tu personaje en este viaje. Él no tiene que aparecer en la escena, tal vez un recuerdo con ellos será suficiente.";

"A provação" = "La prueba";
"O momento que irá definir viver ou morrer. Hora de enfrentar os piores medos do seu personagem." = "El momento que definirá la 'vida' o la 'muerte'. Es el momento de afrontar los peores miedos de tu personaje.";

"O resultado" = "El resultado";
"Como a provação mudou seu personagem? Ele conseguiu o que queria ou falhou?" = "¿Cómo la prueba cambió su carácter? ¿Obtuvo lo que quería o falló?";

"O clímax" = "El clímax";
"Quando tudo parece decidido, vem uma reviravolta. O momento em que seu personagem está prestes a perder o que conquistou até agora." = "Cuando todo parece decidido, llega un cambio. El momento en el que tu personaje está a punto de perder lo que ha conseguido hasta ahora.";

"O final da jornada" = "El final del viaje";
"O que foi conquistado ao final? Como seu personagem está agora?" = "¿Qué se logró al final? ¿Cómo está tu personaje ahora?";

"As consequências" = "Las consecuencias";
"Aproveite para fechar todos os pontos da história que você deixou em aberto. Mostre como essa jornada afetou o mundo em volta do personagem." = "Aprovecha para cerrar todos los puntos de la historia que dejaste abiertos. Muestre cómo este viaje afectó al mundo que rodea al personaje.";


// Explicações

"Explicação1" = "Esta fase se basa en el método Story Spine, ampliamente utilizado por Pixar. Fue creado por Kenn Adams y te permite crear una historia completa con unas pocas frases.

Es un gran primer paso para crear y visualizar los puntos principales de su narrativa.

¿Vamos a intentarlo?";


"Explicação2" = "¡Usted ha llegado a la fase 2! ¡¡Muy bien!!

A partir de aquí, desarrollará un poco más de la narrativa de la historia que creó en la fase 1.

Esta fase fue diseñada para que tomes los elementos clave de tu historia y los aumentes un poco más.

¡Esta historia será increíble!";


"Explicação3" = "¡Está casi terminado! ¡Esta será la mejor historia de todas! Escribes muy bien ¿sabías? Felicitaciones por alcanzar la etapa 3.

Estamos en la recta final. Ahora es el momento de dar vida a este maravilloso universo que has desarrollado. Es en esta fase que describirás en detalle a tu personaje principal, el mundo en el que vive, tus amigos, y también cómo encontró y resolvió todos los problemas y desafíos que habías creado en las fases anteriores. Esta es la versión final de tu historia.

¡¡Ya puedo sentir una gran historia en camino !! ¡Vamos allá!";
