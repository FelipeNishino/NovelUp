//
//  ViewModel.swift
//  NovelUp
//
//  Created by Rodrigo Carvalho on 11/11/20.
//

import SwiftUI
import CoreData

class HistoriaViewModel : ObservableObject {
    
    @Environment(\.managedObjectContext) private var viewContext
    @Published private(set) var historiaModel : [History] = HistoriaViewModel.fetch()
    
    private static func fetch () -> [History]{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: History.entity().name!)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
        do {
            let coreDataItems = try PersistenceController.shared.container.viewContext.fetch(fetchRequest) as? [History]
            if let coreDataItems = coreDataItems {
                if coreDataItems.count != 0 {return coreDataItems }
            }
        }
        catch {
            print("Error: Couldn't load [Item] from Core Data Store")
        }
        return [History]()
    }
    
    func addItem(context: NSManagedObjectContext) {
        let title : String = NSLocalizedString("História", comment: "")
        let imageData = UIImage.pngData(UIImage(named: "NPreto")!)
        let newHistory = History(context: context)
        newHistory.id = UUID()
        newHistory.creationDate = Date()
        newHistory.title = "\(title)\(historiaModel.count)"
        newHistory.historyProgress = .phaseOne
        newHistory.synopsis = nil
        newHistory.genre = nil
        newHistory.tags = nil
        newHistory.image = imageData()
        newHistory.lastViewedPage = 1
        for i in 1...3 {

            let newPage = Page(context: context)
            newPage.id = Int16(i)
            newPage.size = Int16(sizes[i-1])
            newPage.etapasCompletas = 0
            for j in 1...newPage.size {
                let newCell = TextCell(context: context)
                newCell.content = ""
                newCell.isFinished = false
                newCell.id = j
                newCell.label = getDictValue(fase: newPage.id, cell: j, info: .descricao)
                newCell.tip = getDictValue(fase: newPage.id, cell: j, info: .dica)
                newCell.fromPage = newPage
                newPage.cells?.adding(newCell)
            }
            
            newPage.fromHistory = newHistory
            newHistory.pages?.adding(newPage)
        }
        saveContext(context: context)
        historiaModel.append(newHistory)
    }
    
    func getDictValue(fase: Int16, cell: Int16, info: Fases.Info) -> String {
        let key = String(fase) + String(cell)
        return Fases.descricaoDica[key]![info.rawValue]
    }
    
    func getCell(inPage page: Page, cell index: Int) -> TextCell {
        let celula = page.cells!.first(where: { cell in (cell as! TextCell).id == index + 1 }) as! TextCell
        return celula
    }
    
    
    func changePage(paginaAtual: Int16, next: Bool) -> Int {
        var novaPagina : Int = 0
        
        if next && paginaAtual <= 2 {
            DispatchQueue.main.sync { [self] in
                objectWillChange.send()
                novaPagina = Int(paginaAtual + 1)
            }
            return novaPagina
        }
        if !next && paginaAtual >= 2 {
            DispatchQueue.main.sync { [self] in
                objectWillChange.send()
                novaPagina = Int(paginaAtual - 1)
            }
            return novaPagina
        }
        return Int(paginaAtual)
    }
    
    func fetchPages(fromHistoryWithId predicate: UUID) -> [Page] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Page")
        fetchRequest.sortDescriptors = [NSSortDescriptor(keyPath: \Page.id, ascending: true)]
//        fetchRequest.predicate = NSPredicate(format: "fromHistory.id == %@", predicate as CVarArg)
        fetchRequest.predicate = NSPredicate(format: "%K.id == %@", #keyPath(Page.fromHistory), predicate as CVarArg)
        do {
            let coreDataItems = try PersistenceController.shared.container.viewContext.fetch(fetchRequest) as? [Page]
            if let coreDataItems = coreDataItems {
                if coreDataItems.count != 0 { return coreDataItems }
            }
        }
        catch {
            print("Error: Couldn't load [Item] from Core Data Store")
        }
        
//        paginas.forEach({pagina in print("fetched pages history id: \(pagina.fromHistory?.id), predicate id: \(predicate)")})
//        return [Page]()
//
//                let fetchRequest = FetchRequest<Page>(
//                    sortDescriptors: [NSSortDescriptor(keyPath: \Page.id, ascending: true)],
//                    predicate: NSPredicate(format: "fromHistory.id == %@", predicate as CVarArg),
//                    //predicate: NSPredicate(format: "%K == %@", #keyPath(fromHistory.id), predicate as CVarArg),
//                    animation: .default
//                )
//
//                return fetchRequest.wrappedValue.compactMap({$0})
        return [Page]()
    }
    
    func saveContext(context: NSManagedObjectContext){
        do {
            try context.save()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
    
    func deleteItems(context: NSManagedObjectContext, index: Int) {
        let historia = historiaModel[index]
        context.delete(historia)
        historiaModel.remove(at: index)
        saveContext(context: context)
        //historiaModel = HistoriaViewModel.fetch()
    }
    
}
