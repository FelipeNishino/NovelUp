//
//  LaunchScreen.swift
//  NovelUp
//
//  Created by Gustavo Rigor on 04/11/20.
//

import SwiftUI

struct LaunchScreen: View {
    @State private var mostrarTelaPrincipal = false
    var body: some View {
        Group{
            if mostrarTelaPrincipal{
                ContentView()
            } else {
                ZStack{
                    Color("launch")
                        .edgesIgnoringSafeArea(.all)
                    Image("logoTEMP")
                        .resizable()
                        .frame(width: 282, height: 250, alignment: .center)
                }
            }
        }
        .onAppear{
            withAnimation(.linear(duration: 1)){
                mostrarTelaPrincipal = true
            }
        }
    }
}

struct LaunchScreen_Previews: PreviewProvider {
    static var previews: some View {
        LaunchScreen()
    }
}
