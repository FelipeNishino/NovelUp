//
//  HistoriaView.swift
//  NovelUp
//
//  Created by Gustavo Rigor on 27/10/20.
//

import SwiftUI

struct HistoriaView : View {
    // TODO: (Rever) Carregar os valores do dicionário assincronamente, atualmente trava a UI.
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.managedObjectContext) private var viewContext
    @State var section : Section
    @State var fimHistoria : Bool = false
    @State var toggleExplicacao : Bool = false
    @State var mostrarDica = false
    @State var tituloDica:String = "Ops :("
    @State var mensagemDica: String = "Ainda estamos trabalhando nessa função."
    @State var etapasCompletas : Int = 0
    @State var hidePrevButton : Bool
    @State private var visibilityControlsFromBundle : [String:Bool]
    private var viewModel : HistoriaViewModel
    private var historia : History
    private var fases: [Fase_] = [.init(id: 1, etapas: 7, etapasCompletas: 7), .init(id: 2, etapas: 9, etapasCompletas: 9), .init(id: 3, etapas: 13, etapasCompletas: 13)]
    
    init(viewModel: HistoriaViewModel, historia: History) {
        _visibilityControlsFromBundle = State(initialValue:
                                                [
                                                    "1" : SettingsBundle.showPhase1,
                                                    "2" : SettingsBundle.showPhase2,
                                                    "3" : SettingsBundle.showPhase3
                                                ])
        self.viewModel = viewModel
        self.historia = historia
        self._hidePrevButton = State(initialValue: historia.lastViewedPage == 1)
        _section = State(initialValue:Section(index: 0))
        _etapasCompletas = State(initialValue: Int(getPage().etapasCompletas))
    }
    
    var body: some View {
        NavigationView {
            if !fimHistoria {
                VStack {
                    switch section {
                    case .description:
                        Text(Fases.explicacaoFase[Int(historia.lastViewedPage) - 1])
                            .multilineTextAlignment(.leading)
                            .padding(.top, 20)
                            .padding(20)
                        Spacer()
                        Toggle("Não mostrar novamente", isOn: $toggleExplicacao).toggleStyle(SwitchToggleStyle(tint: Color.init("Background")))
                            .padding(20)

                    case .write:
                        ScrollView {
                            VStack {
                                ForEach((historia.pages?.filter({($0 as! Page).id == historia.lastViewedPage}).first as! Page).cells!.sortedArray(using: [NSSortDescriptor(key: "id", ascending: true)]) as! [TextCell], id: \.self) { cell in
                                    CellView(cell: cell, action: {newValue in self.etapasCompletas += newValue ? 1 : -1; cell.isFinished = newValue}, {
                                        tituloDica = cell.label!
                                        mensagemDica = cell.tip!
                                        mostrarDica.toggle()
                                    }, historia.lastViewedPage == 1)
                                    .padding(.leading,1)
                                    .padding(.trailing,2)
                                }
                            }.padding(20)
                        }
                    case .read:
                        ScrollView {
                            VStack {
                                ForEach((historia.pages?.filter({($0 as! Page).id == historia.lastViewedPage}).first as! Page).cells!.sortedArray(using: [NSSortDescriptor(key: "id", ascending: true)]) as! [TextCell], id: \.self) { cell in
                                    if (historia.lastViewedPage == 1 ){
                                        HStack{
                                            Text("    " + cell.label! + cell.content!).multilineTextAlignment(.leading)
                                            Spacer()
                                        }
                                    }else{
                                        HStack{
                                            Text("    " + cell.content!).multilineTextAlignment(.leading)
                                            Spacer()
                                        }
                                    }
                                    
                                }
                            }.padding(20)
                        }
                    }
                    HStack {
                        if !hidePrevButton {
                            Button("Anterior") {
                                if section == .description {
                                    visibilityControlsFromBundle[String(getPage().id)]! = !toggleExplicacao
                                }
                                viewModel.saveContext(context: viewContext)
                                if section.changeSection(prev: true, fase: Int(historia.lastViewedPage), showDescription: !toggleExplicacao, visibilityControls: visibilityControlsFromBundle) {
                                    
                                    getPage().etapasCompletas = Int16(self.etapasCompletas)
                                    DispatchQueue.global(qos: .userInteractive).async {
                                        historia.lastViewedPage = Int16(viewModel.changePage(paginaAtual: historia.lastViewedPage, next: false))
                                        self.etapasCompletas = Int(getPage().etapasCompletas)
                                    }
                                    hidePrevButton = false
                                } else {
                                    print(getPage().id, section.rawValue, visibilityControlsFromBundle["1"]!)
                                    if ((getPage().id == 1 && section == .description) || (getPage().id == 1 && section == .write && !visibilityControlsFromBundle["1"]!)) {
                                        hidePrevButton = true
                                    }
                                }
                                if !((getPage().id == 1 && section == .description) || (getPage().id == 1 && section == .write && !visibilityControlsFromBundle["1"]!)) {
                                    hidePrevButton = false
                                }
                                toggleExplicacao = false
                            }.frame(width: 85.0, height:40.0).greenify()
                            .padding(.leading, 20)
                            .padding(.vertical, 20)
                        }
                        Spacer()
                        Button((historia.lastViewedPage == 3 && section == .read ? "Finalizar" : "Próxima")) {
                            if section == .description {
                                visibilityControlsFromBundle[String(getPage().id)]! = !toggleExplicacao
                            }
                            viewModel.saveContext(context: viewContext)
                            fimHistoria = historia.lastViewedPage == 3 && section == .read
                            getPage().etapasCompletas = Int16(self.etapasCompletas)
                            if section.changeSection(prev: false, fase: Int(historia.lastViewedPage), showDescription: !toggleExplicacao, visibilityControls: visibilityControlsFromBundle) {
                                DispatchQueue.global(qos: .userInitiated).async {
                                    historia.lastViewedPage = Int16(viewModel.changePage(paginaAtual: historia.lastViewedPage, next: true))
                                    self.etapasCompletas = Int(getPage().etapasCompletas)
                                }
                                hidePrevButton = false
                            } else {
                                if ((getPage().id == 1 && section == .description) || (getPage().id == 1 && section == .write && !visibilityControlsFromBundle["1"]!)) {
                                    hidePrevButton = true
                                }
                            }
                            if !((getPage().id == 1 && section == .description) || (getPage().id == 1 && section == .write && !visibilityControlsFromBundle["1"]!)) {
                                hidePrevButton = false
                            }
                            toggleExplicacao = false
                        }.frame(width: 85.0, height: 40.0).greenify().disabled(self.etapasCompletas != getPage().size && section == .write)
                        .padding(.trailing, 20)
                        .padding(.vertical, 20)
                    }
                }
                
                .frame(maxWidth: UIScreen.main.bounds.size.width, maxHeight: UIScreen.main.bounds.size.height)
                .navigationTitle(NSLocalizedString("Fase ", comment: "") + String(Int(historia.lastViewedPage)))
                //Text(NSLocalizedString("Nº Palavras: ", comment: "") + String(Int(numPalavras)))
                .navigationBarTitleDisplayMode(.inline)
                .onTapGesture {
                    self.hideKeyboard()
                }
                .navigationBarItems(trailing: Button("Sair"){
                                        viewModel.saveContext(context: viewContext)
                                        presentationMode.wrappedValue.dismiss() })
                //.padding(20)
            } else {
                EditarHistorias(historia: historia, editando: false, function: { fimHistoria = false })
                    .navigationTitle("\(historia.title!)")
                    .navigationBarTitleDisplayMode(.inline)
                    .navigationBarItems(trailing: Button("Salvar"){
                                            viewModel.saveContext(context: viewContext)
                                            presentationMode.wrappedValue.dismiss() })
            }
        }
        .alert(isPresented: $mostrarDica) {
            Alert(title: Text(tituloDica), message: Text(mensagemDica).font(.title), dismissButton: .default(Text("Fechar")))
        }
        .accentColor(.init("Background"))
    }
    
    func getPage() -> Page {
        (historia.pages?.filter({($0 as! Page).id == historia.lastViewedPage}).first as! Page)
    }
}

struct CellView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @State private var cell : TextCell
    private var mostrarDica: () -> Void
    private var tamanhoCaixaDeTexto: Bool
    private var changeCounter: (Bool) -> Void
    
    // https://stackoverflow.com/questions/56691630/swiftui-state-var-initialization-issue
    init(cell: TextCell, action: @escaping (Bool) -> Void, _ dica: @escaping () -> Void, _ tamanho: Bool) {
        _cell = State(initialValue: cell)
        mostrarDica = dica
        tamanhoCaixaDeTexto = tamanho
        self.changeCounter = action
    }
    
    var body : some View {
        VStack {
            HStack {
                Text(cell.label!)
                Spacer()
                Button(action: mostrarDica) {
                    Image(systemName: "questionmark.circle")
                        .renderingMode(.template)
                        .foregroundColor(Color("tipColor"))
                }
            }
            
            ZStack {
                RoundedRectangle(cornerRadius: 1.0).stroke(Color.black.opacity(0.2), lineWidth: 1.0)
                
                    if tamanhoCaixaDeTexto{
                        TextEditor(text: $cell.content ?? "default value")
                            .frame(height: 50)
                            .multilineTextAlignment(.leading)
                            .border(Color("BorderTextEditor"), width: 0.5)
                    }else{
                        TextEditor(text: $cell.content ?? "default value")
                            .frame(height: 75)
                            .multilineTextAlignment(.leading)
                            .border(Color("BorderTextEditor"), width: 0.5)
                    }
                    
            }
            
                ToggleDemo(isFinished: cell.isFinished, action: changeCounter)

//                .frame(minWidth: 57, idealWidth: 57, maxWidth: 57, alignment: .leading)
//                Toggle("Finalizado", isOn: $cell.isFinished).onReceive(, perform: {print("a")})
                //                Toggle("Finalizado", isOn: $cell.isFinished).onReceive(, perform: {print("a")})
            
        }.padding(.bottom)
        
        //        if mostrarDica {
        //            PopUp(mostrar: $mostrarDica, Titulo: "Ops :(", Mensagem: "Ainda estamos trabalhando nessa função.")
        //                .edgesIgnoringSafeArea(.all)
        //        }
        
    }
}
// https://www.iditect.com/how-to/58214478.html
struct ToggleDemo: View {
    @State private var isToggled : Bool
    private var changeCounter : (Bool) -> Void
    
    init(isFinished: Bool, action: @escaping (Bool) -> Void) {
        _isToggled = State(initialValue: isFinished)
        self.changeCounter = action
    }
    
    var body: some View {
        let binding = Binding(
            get: { self.isToggled },
            set: {
                potentialAsyncFunction($0)
            }
        )
        
        func potentialAsyncFunction(_ newState: Bool) {
            changeCounter(newState)
            self.isToggled = newState
        }
        
        return Toggle(isOn: binding) {
            HStack{
                Spacer()
                Text("Finalizado")
            }
        }.toggleStyle(SwitchToggleStyle(tint: Color.init("Background")))
    }
}

// https://stackoverflow.com/questions/57021722/swiftui-optional-textfield
func ??<T>(lhs: Binding<Optional<T>>, rhs: T) -> Binding<T> {
    Binding(
        get: { lhs.wrappedValue ?? rhs },
        set: { lhs.wrappedValue = $0 }
    )
}

struct Fase_: Identifiable {
    var id: Int
    var etapas:Int
    var etapasCompletas: Int
    // Colocar a fase do banco de dados salva aqui
}
//
//struct HistoriaView_Previews: PreviewProvider {
//    static var previews: some View {
//        HistoriaView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
//    }
//}
