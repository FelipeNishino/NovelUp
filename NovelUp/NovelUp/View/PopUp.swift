//
//  popup.swift
//  NovelUp
//
//  Created by Gustavo Rigor on 29/10/20.
//

import SwiftUI


struct PopUp: View{
    
    //Colocar a variavel a observar
    //Titulo e a mensagem a ser mostrada
    
    @Binding var mostrar : Bool
    
    var Titulo: String
    var Mensagem: String
    
    var body: some View{
        ZStack(alignment: Alignment(horizontal: .trailing, vertical: .top)){
            
            VStack(alignment:.leading ,spacing: 1){
                
                Text(Titulo)
                    .padding(.leading)
                    .multilineTextAlignment(.leading)
                    .frame(width: 240.0, height: 35.0, alignment: .leading)
                    .background(Color("Background").opacity(0.9), alignment: .center)
                    .font(.title2)
                    .border(Color("Background").opacity(1), width: 3)
                
                Text(Mensagem)
                    .padding()
                    .multilineTextAlignment(.leading)
                    .frame(width: 280.0)
                    .background(Color.white.opacity(1), alignment: .center)
                //.border(Color.gray.opacity(0.6), width: 3)
                
            }
            
            Button(action: {
                withAnimation {
                    mostrar.toggle()
                }
            }) {
                Image(systemName: "xmark")
                    .frame(width: 38.0, height: 35.0, alignment: .center)
                    .foregroundColor(.black)
                    .background(Color("Background").opacity(1), alignment: .center)
                    .cornerRadius(5)
                
            }
        }.frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color("launch").opacity(0.5).onTapGesture{
            withAnimation{
                mostrar.toggle()
            }
        })
        
    }
    
}
