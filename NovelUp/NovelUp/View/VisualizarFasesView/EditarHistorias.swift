//
//  EditarHistorias.swift
//  NovelUp
//
//  Created by Gustavo Rigor on 10/11/20.
//

import SwiftUI

struct EditarHistorias: View {
    
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.managedObjectContext) private var viewContext
    
    var historia : History
    
    let generos = ["",
                   NSLocalizedString("Ação", comment: ""),
                   NSLocalizedString("Aventura", comment: ""),
                   NSLocalizedString("Comédia", comment: ""),
                   NSLocalizedString("Drama", comment: ""),
                   NSLocalizedString("Fantasia", comment: ""),
                   NSLocalizedString("Ficção", comment: ""),
                   NSLocalizedString("Ficção Científica", comment: ""),
                   NSLocalizedString("Mistério", comment: ""),
                   NSLocalizedString("Poesia", comment: ""),
                   NSLocalizedString("Policial", comment: ""),
                   NSLocalizedString("Romance", comment: ""),
                   NSLocalizedString("Suspense", comment: ""),
                   NSLocalizedString("Terror", comment: ""),
                   NSLocalizedString("Tragédia", comment: "")]
    
    @State private var generosIndex = 0
    @State private var adicionarImagem:Bool = false
    @State private var inputImage: UIImage?
    @State private var image: Image?
    @State var editando:Bool
    @State private var titulo : String = ""
    @State private var sinopse : String = ""
    @State private var tags : String = ""
    @State private var synopsisOpacity = 0.25
    
    var function : () -> Void
    
    var body: some View {
        NavigationView{
            ScrollView {
            VStack {
                
                VStack {
                    
                    if image != nil {
                        image?
                            .resizable()
                            .scaledToFit()
                            .frame(width: 175, height: 175, alignment: .center)
                            .padding(.bottom ,3)
                    } else {
                        Image.init(data: historia.image, placeholder: "NVerde")
                            .resizable()
                            .frame(width: 175, height: 175, alignment: .center)
                            .padding(.bottom ,3)
                    }
                    Button(action: {
                        adicionarImagem.toggle()
                    },
                    label: {
                        Image(systemName: "plus.circle")
                        Text(LocalizedStringKey("Alterar Imagem"))
                    })
                }
                
                VStack(alignment: .leading){
                    Text(LocalizedStringKey("Título:"))
                        .multilineTextAlignment(.center)
                        .padding(.bottom,5)
                    
                    TextField(historia.title ?? "Escreva um título", text: $titulo)
                        .frame(width: 400, height: 30)
                        .padding(.leading, 8)
                        .border(Color.gray)
                        
                    Text(LocalizedStringKey("Sinopse:"))
                    // https://stackoverflow.com/questions/62741851/how-to-add-placeholder-text-to-texteditor-in-swiftui
                    ZStack {
                        if sinopse.isEmpty {
                            Group {
                                HStack{
                                    Text(historia.synopsis ?? "Escreva uma sinopse")
                                        
                                        .multilineTextAlignment(.leading)
                                        .padding(.top, 6)
                                        .padding(.bottom, 5)
                                        .padding(.leading, 12)
                                        .lineLimit(nil)
                                        .foregroundColor(Color.init(synopsisOpacity > 0.25 ? UIColor.lightGray : UIColor.darkGray))
                                    Spacer()
                                }
                            }.frame(height: 60, alignment: .topLeading)
                        }
                        TextEditor(text: $sinopse)
                            .frame(height: 60.0)
                            .multilineTextAlignment(.leading)
                            .padding(.bottom, 5)
                            .padding(.leading, 8)
                            .opacity(sinopse.isEmpty ? synopsisOpacity : 1)
                            .border(Color.gray)

//                            .onTapGesture {
//                                synopsisOpacity += synopsisOpacity>0.25 ? -0.5 : 0.5
//                            }
                    }
                    
                    Text(LocalizedStringKey("Gênero:"))
                    
                    Picker(selection: $generosIndex, label: Text("Selecionar gênero")) {
                        ForEach(0 ..< generos.count) {
                            Text(self.generos[$0])
                        }
                    }
                    .frame(width: 400, height: 150, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    Text(LocalizedStringKey("Tags:"))
                        .padding(.bottom, 5)
                    TextField(historia.tags ?? "Insira tags na sua história", text: $tags)
                        .padding(.leading, 8)
                        .multilineTextAlignment(.leading)
                        .padding(.bottom, 5)
                        .border(Color.gray)
                }
                .padding(20)
                
                HStack {
                    if !editando {
                        Button("Anterior", action: function)
                            .frame(width: 75.0, height: 35.0)
                            .greenify()
                            .padding(.leading, 25.0)
                        
                        Spacer()
                        /*<<<<<<< HEAD
                         } else {
                         Button("Salvar Alterações"){
                         =======*/
                    } else{
                        Button(LocalizedStringKey("Salvar Alterações")){
                            //>>>>>>> acessibilidade
                            save()
                            self.presentationMode.wrappedValue.dismiss()
                        }
                        .frame(width: 200, height: 40)
                        .greenify()
                        .navigationBarItems(trailing:
                                                Button("Cancelar"){
                                                    presentationMode.wrappedValue.dismiss()
                                                })
                        .navigationBarTitleDisplayMode(.inline)
                    }
                }
                .onDisappear() {
                    if !editando{
                        save()
                    }
                    
                }
                .sheet(isPresented: $adicionarImagem, onDismiss: loadImage) {
                    ImagePicker(image: self.$inputImage)
                }
            }
            .navigationBarHidden(!editando)
            //.navigationTitle("Editar História")
        }
            .frame(maxWidth: UIScreen.main.bounds.width, maxHeight: UIScreen.main.bounds.height)
            .padding(.top, 20)
        .onTapGesture {
            self.hideKeyboard()
        }
        }
        .accentColor(.init("Background"))
    }
    
    func loadImage() {
        guard let inputImage = inputImage else { return }
        image = Image(uiImage: inputImage)
    }
    
    func save() {
        historia.updateTitle(title: titulo)
        historia.updateSinopse(sinopse: sinopse)
        historia.updateGenre(genero: generos[generosIndex])
        historia.updateTags(tags: tags)
        historia.updateImage(imagem: inputImage)
        try? viewContext.save()
    }
    
}

/*
 struct EditarHistorias_Previews: PreviewProvider {
 static var previews: some View {
 let historia : History()
 historia.title = "Teste"
 historia.synopsis = "Teste Teste"
 
 EditarHistorias(historia: historia,editando: true)
 }
 }
 */
