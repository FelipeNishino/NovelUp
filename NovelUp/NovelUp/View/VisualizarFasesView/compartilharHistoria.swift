//
//  compartilharHistoria.swift
//  NovelUp
//
//  Created by Gustavo Rigor on 03/11/20.
//

import SwiftUI

func compartilharHistoria(_ historia: [Any]){
    let compartilhamentoHistoria = UIActivityViewController(activityItems: historia, applicationActivities: nil)
    UIApplication.shared.windows.first?.rootViewController?.present(compartilhamentoHistoria, animated: true, completion: nil)
}

