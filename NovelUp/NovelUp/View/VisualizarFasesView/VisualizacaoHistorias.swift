//
//  VisualizacaoHistorias.swift
//  NovelUp
//
//  Created by Gustavo Rigor on 29/10/20.
//

import SwiftUI
import CoreData

struct VisualizacaoHistoria : View {
    
    @State var trocarTela = false
    //@State var selecionaTela:Bool = false //False = Editar & True = História
    @ObservedObject var historia : History
    var viewModel : HistoriaViewModel
    var fases: [Fase]
    
    
    init(historia: History, viewModel: HistoriaViewModel) {
        self.historia = historia
        self.viewModel = viewModel
        let temp = VisualizacaoHistoria.verificarProgresso(historia)
        fases = [.init(id: 1, etapas: 8, etapasCompletas: temp["Fase1"]!), .init(id: 2, etapas: 9, etapasCompletas: temp["Fase2"]!), .init(id: 3, etapas: 12, etapasCompletas: temp["Fase3"]!)]
    }
    
    
    //var fases: [Fase] = [.init(id: 1, etapas: 7, etapasCompletas: 7), .init(id: 2, etapas: 9, etapasCompletas: 9), .init(id: 3, etapas: 13, etapasCompletas: 4)]
    let  mensagemSinopse = "A sinopse ainda não foi escrita"
    let  mensagemGenero = "Gênero ainda não definido"
    let  mensagemTag = "Nenhuma tag definida"
    let  labelFase = "Fase"
    
    var body: some View {
        
        ScrollView {
            VStack{
                //Spacer()
                VStack{
                    Text(historia.title ?? "Sem título")
                        .font(.title)
                        .fontWeight(.regular)
                        .multilineTextAlignment(.center)
                    
                    Image.init(data: historia.image, placeholder: "Teste")
                        .resizable()
                        .frame(width: 200, height: 200, alignment: .center)
                        .padding(8)
                }
                HStack{
                    
                    VStack(alignment: .leading){
                        Text(LocalizedStringKey("Sinopse:"))
                            .font(.system(size: 18, weight: .bold, design: .default))
                            .padding(.leading, 18)
                        
                        Text(historia.synopsis ?? NSLocalizedString(mensagemSinopse, comment: ""))
                            //.frame(height: 80.0)
                            //.lineLimit(20)
                            .fixedSize(horizontal: false, vertical: true)
                            .multilineTextAlignment(.leading)
                            .padding(.top, 18)
                            .padding(.leading, 23)
                            .padding(.bottom, 6)
                        
                        Text(LocalizedStringKey("Gênero:")).padding(.bottom, 8)
                            .font(.system(size: 18, weight: .bold, design: .default))
                            .padding(.leading, 18)
                        Text(historia.genre ?? NSLocalizedString(mensagemGenero, comment: ""))
                            .multilineTextAlignment(.leading)
                            .padding(.leading, 23)
                            .padding(.bottom, 6)
                        
                        Text(LocalizedStringKey("Tags:")).padding(.bottom, 8)
                            .font(.system(size: 18, weight: .bold, design: .default))
                            .padding(.leading, 18)
                            .padding(.top, 18)
                        Text(historia.tags ?? NSLocalizedString(mensagemTag, comment: ""))
                            .multilineTextAlignment(.leading)
                            .lineLimit(nil)
                            .padding(.leading, 23)
                            .padding(.bottom, 6)
                        
                        
                        
                    }
                    //Spacer()
                }.padding(20)
                
                //List{
                ForEach(fases){ fase in
                    
                    if fase.id == 3 {
                        HStack{
                            Text(NSLocalizedString("Fase ",comment: "") + String(Int(fase.id)))
                                .padding(.leading,18)
                            Spacer()
                            //Text("\(fase.etapas) / \(fase.etapasCompletas)")
                            ProgressView(value: (fase.etapasCompletas), total: (fase.etapas))
                                .accentColor(Color("Background"))
                                .frame(width:100)
                                .padding(.trailing, 18)
                            
                        }
                    } else {
                        HStack{
                            Text(NSLocalizedString("Fase ",comment: "") + String(Int(fase.id)))
                                .padding(.leading,18)
                            Spacer()
                            //Text("\(fase.etapas) / \(fase.etapasCompletas)")
                            ProgressView(value: (fase.etapasCompletas), total: (fase.etapas))
                                .accentColor(Color("Background"))
                                .frame(width:100)
                                .padding(.trailing, 18)
                            
                        }
                        Divider()
                        
                    }
                    
                }
                //}
                
                Button("Escrever História") {
                    selecionaTela = true
                    trocarTela = true
                }.frame(width: 200, height: 40).greenify()
            }
        }
        .fullScreenCover(isPresented: $trocarTela, content:{
            if selecionaTela {
                HistoriaView(viewModel: viewModel, historia: historia)
            }else{
                EditarHistorias(historia: historia, editando: true, function: {  })
            }
        })
            .padding(.top, 15)
                .navigationBarTitleDisplayMode(.inline)
                .navigationBarItems(trailing:
                                        HStack{
                                            Button(action: {
                                                //TODO: Colocar a fase três da *história* aqui
                                                var compartilhaHistoria:[Any] = []
                                                compartilhaHistoria.removeAll()
                                                compartilhaHistoria.append(UIImage(data: historia.image!) ?? UIImage(named: "NPreto")!)
                                                var str = historia.title ?? "Sem título :("
                                                str += "\n"
                                                str += parte3(historia)
                                                compartilhaHistoria.append(str)
                                                NovelUp.compartilharHistoria(compartilhaHistoria)
                                                
                                            }, label: {
                                                Image(systemName: "square.and.arrow.up")
                                                    .renderingMode(.template)
                                                    .foregroundColor(.gray)
                                            })
                                            
                                            .padding(12)
                                            Button(action: {
                                                selecionaTela = false
                                                trocarTela = true
                                                
                                            }, label: {
                                                Image(systemName: "square.and.pencil")
                                                    .renderingMode(.template)
                                                    .foregroundColor(.gray)
                                            })
                                            
                                            
                                        })
        }
        
        
        static private func verificarProgresso(_ historia:History)->[String:Float]{
            var count:Float = 0
            var fase:[Int:Float] = [1:0.0,2:0.0,3:0.0]
            
            for n in 1...3{
                for cell in ((historia.pages?.filter({($0 as! Page).id == n}).first as! Page).cells!.sortedArray(using: [NSSortDescriptor(key: "id", ascending: true)]) as! [TextCell]) {
                    count = cell.isFinished ? count + 1.0 : count
                }
                fase[n] = count
                count = 0
            }
            
            return ["Fase1": fase[1]!, "Fase2": fase[2]!, "Fase3": fase[3]!]
        }
        
        private func parte3(_ historia:History)->String{
            var hist:String = ""
            for cell in ((historia.pages?.filter({($0 as! Page).id == 3}).first as! Page).cells!.sortedArray(using: [NSSortDescriptor(key: "id", ascending: true)]) as! [TextCell]) {
                hist = hist + cell.content! + "\n"
            }
            return hist
        }
        
    }
    var selecionaTela:Bool = false
    struct Fase: Identifiable {
        var id: Int
        var etapas:Float
        var etapasCompletas: Float
        // Colocar a fase do banco de dados salva aqui
    }

/*
 struct VisualizacaoHistoria_Previews: PreviewProvider {
 static var previews: some View {
 VisualizacaoHistoria(historia:).environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
 }
 }
 */
