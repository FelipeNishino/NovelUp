//
//  botaoVisualizarHistoria.swift
//  NovelUp
//
//  Created by Gustavo Rigor on 05/11/20.
//

import SwiftUI

struct botaoVisualizarHistoria: View {
    var image:Image
    let titulo:String
    let temFoto:Bool
    //let numeroPalavras = "??" //Adicionar um atributo a History informado a quantidade de palavras, sendo inicializado como um opcional

    let progresso : [String:Float]
    //let dataCriacao: Date?

    //let progresso = ["Fase1":7.0, "Fase2":9.0, "Fase3": 4.0]
    let dataCriacao: String //mudou aqui

    let progress:String = "Blá"
    let numPalavras:Float
    let id: UUID = UUID()


    init(_ hist: History) {
        titulo = hist.title ?? "Sem título"
        dataCriacao = formatoData(data: hist.creationDate!)
        //Text("Item at \(item.timestamp!, formatter: itemFormatter)")
        //progresso = hist.historyProgress  Verificar pq Progress é do tipo String
        temFoto = false //Adicionar um atributo a History informando se tem ou não foto. default: false.
        image = Image.init(data: hist.image, placeholder: "NVerde")
        let temp = botaoVisualizarHistoria.contarPalavras(hist)
        numPalavras = temp["palavras"]!
        progresso = ["Fase1":temp["Fase1"]!, "Fase2":temp["Fase2"]!, "Fase3": temp["Fase3"]!]
    }
    
    var body: some View {
        HStack{
            
            image
                .resizable()
                .frame(width: 60, height: 60, alignment: .center)
                .foregroundColor(Color("TextColor"))
                .padding(5)
                .background(Color("BackgroundPhoto"))
                //.border(Color("BorderPhoto"), width: 1)
                
            VStack(alignment: .leading){
                Text(titulo)
                    .font(.system(size: 20, weight: .bold, design: .default))
                    .foregroundColor(Color("TextColor"))
                Text(NSLocalizedString("Nº Palavras: ", comment: "") + String(Int(numPalavras)))
                    .font(.system(size: 12, design: .default))
                    .foregroundColor(Color("TextColor"))
                Text(NSLocalizedString("Data de criação: ", comment: "") + dataCriacao)
                    .font(.system(size: 12, design: .default))
                    .foregroundColor(Color("TextColor"))
                HStack{
                    ProgressView(value: progresso["Fase1"], total: 8.0)
                        .accentColor(Color("Background"))
                    ProgressView(value: progresso["Fase2"], total: 9.0)
                        .accentColor(Color("Background"))
                    ProgressView(value: progresso["Fase3"], total: 12.0)
                        //.padding(.trailing)
                        .accentColor(Color("Background"))
                }
            }
            .padding(15)
        }
        .padding(15)
        .frame(width: 350, height: 100)
        .background(Color("BackgroundView"))
        //.border(Color("BorderButton"), width: 1)
    }
    
    static private func contarPalavras(_ historia:History)->[String:Float]{
        var temp = ""
        var qtd:Float = 0.0
        var count:Float = 0
        var fase:[Int:Float] = [1:0.0,2:0.0,3:0.0]
        
        for n in 1...3{
            for cell in ((historia.pages?.filter({($0 as! Page).id == n}).first as! Page).cells!.sortedArray(using: [NSSortDescriptor(key: "id", ascending: true)]) as! [TextCell]) {
                temp = cell.content!
                count = cell.isFinished ? count + 1.0 : count
                qtd = qtd + Float(temp.split(separator: " ").count)
            }
            fase[n] = count
            count = 0
        }
        
        return ["palavras":qtd, "Fase1": fase[1]!, "Fase2": fase[2]!, "Fase3": fase[3]!]
    }

}


/*
struct botaoVisualizarHistoria_Previews: PreviewProvider {
    static var previews: some View {
        botaoVisualizarHistoria(titulo: "Oi", temFoto: true, dataCriacao: Date())
    }
}
*/

