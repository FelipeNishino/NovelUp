//
//  botaoCriarHistoria.swift
//  NovelUp
//
//  Created by Gustavo Rigor on 27/10/20.
//

import SwiftUI

let sizes = [8, 9, 12]

struct botaoCriarHistoria : View {
    @Environment(\.managedObjectContext) private var viewContext //Talvez isso saia daqui quando MVVM estiver integrado\

    var viewModel : HistoriaViewModel
    var function : () -> Void
    
    var body: some View {

        HStack{
            Image(systemName: "plus")
                .resizable()
                .frame(width: 30, height: 30, alignment: .center)
                .foregroundColor(Color("TextColor"))
                .padding(20)
                .background(Color("BackgroundPhoto"))
                //.border(Color("BorderPhoto"), width: 1)
   
            Button(LocalizedStringKey("Criar nova história"), action: function)
            .font(.system(size: 25, weight: .bold, design: .default))
            .foregroundColor(Color("TextColor"))
            .padding(15)
        }
        .padding(15)
        .background(Color("BackgroundView"))
        //.border(Color("BorderButton"), width: 1)
    }
}

//struct BB: PreviewProvider {
//    static var previews: some View {
//        botaoCriarHistoria(viewModel: HistoriaViewModel()).environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
//    }
//}
