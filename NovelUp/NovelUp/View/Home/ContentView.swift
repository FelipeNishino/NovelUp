//
//  ContentView.swift
//  NovelUp
//
//  Created by Gustavo Rigor on 26/10/20.
//

import SwiftUI
import CoreData

struct ContentView: View {
    
    @Environment(\.managedObjectContext) private var viewContext
    
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \History.creationDate, ascending: true)],
        animation: .default)
    private var historias: FetchedResults<History>
    
    @ObservedObject var viewModel : HistoriaViewModel = HistoriaViewModel()
    @ObservedObject var searchBar : SearchBar = SearchBar()
    @State var escrever : Bool = false
    
    init() {
        if !SettingsBundle.didAlreadyLaunch {
            SettingsBundle.setApplicationDefault()
        }
    }
    
    var body: some View {
        NavigationView{
            List {
                ZStack{
                    botaoCriarHistoria(viewModel: viewModel, function: {
                        viewModel.objectWillChange.send()
                        viewModel.addItem(context: viewContext)
                        escrever.toggle()
                    })
                    NavigationLink(destination: LaunchScreen()){
                    }.disabled(true)
                }
                
                ForEach(historias.filter {
                    searchBar.text.isEmpty || $0.title!.localizedStandardContains(searchBar.text) || $0.tags!.localizedStandardContains(searchBar.text)
                }) { historia in
                    ZStack {
                        botaoVisualizarHistoria(historia)
                        NavigationLink(destination: VisualizacaoHistoria(historia: historia, viewModel: viewModel)){
                        }
                    }
                }
                .onDelete { indexSet in
                    for index in indexSet {
                        viewModel.deleteItems(context: viewContext, index: index)
                    }
                }
            }
            .navigationTitle("Histórias")
            .add(self.searchBar)
        }
        .fullScreenCover(isPresented: $escrever, content:{
            HistoriaView(viewModel: viewModel, historia: viewModel.historiaModel.last!)
        })
        .accentColor(.init("Background"))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
