//
//  Formatter.swift
//  NovelUp
//
//  Created by Rodrigo Carvalho on 11/11/20.
//

import Foundation

func formatoData (data:Date) -> String {
    let formatador = DateFormatter()
    formatador.dateStyle = .short
    formatador.locale = .current
    let template = "MM/dd/yyyy"
    formatador.dateFormat = DateFormatter.dateFormat(fromTemplate: template, options: 0, locale: formatador.locale)!
    return formatador.string(from: data)
}

