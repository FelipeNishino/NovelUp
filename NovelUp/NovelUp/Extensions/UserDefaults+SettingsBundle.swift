//
//  SettingsBundleHelper.swift
//  NovelUp
//
//  Created by Felipe Nishino on 26/11/20.
//

import Foundation

typealias SettingsBundle = UserDefaults

extension UserDefaults {
    private enum SettingsAccessError : String, Error {
        case badKey = "Não existe valor valido para a chave utilizada"
    }
    
    private enum Keys {
        static let phase1 = "showPhase1"
        static let phase2 = "showPhase2"
        static let phase3 = "showPhase3"
        static let didAlreadyLaunch = "didAlreadyLaunch"
        /// Used for String manipulation only. Accessing UserDefaults with this key will generate an error.
        static let baseKey = "showPhase"
    }
    
    class var showPhase1 : Bool {
        get {
            return standard.bool(forKey: Keys.phase1)
        }
        set {
            standard.set(newValue, forKey: Keys.phase1)
        }
    }
    
    class var showPhase2 : Bool {
        get {
            return standard.bool(forKey: Keys.phase2)
        }
        set {
            standard.set(newValue, forKey: Keys.phase2)
        }
    }
    
    class var showPhase3 : Bool {
        get {
            return standard.bool(forKey: Keys.phase3)
        }
        set {
            standard.set(newValue, forKey: Keys.phase3)
        }
    }
    
    class var didAlreadyLaunch : Bool {
        get {
            return standard.bool(forKey: Keys.didAlreadyLaunch)
        }
        set {
            standard.set(newValue, forKey: Keys.didAlreadyLaunch)
        }
    }
    
    static func getOrSetValue(forKey key: Int) -> Bool? {
        if inRange(value: key, range: 1...3) {
            let newKey = Keys.baseKey + String(key)
//            print("chave utilizada: \(newKey), valor: \(UserDefaults.standard.bool(forKey: newKey))")
            return UserDefaults.standard.bool(forKey: newKey)
        }
        
        print("Não existe valor valido para a chave utilizada")
        return nil
    }
    static func getOrSetValue(value: Bool, forKey key: Int) {
        inRange(value: key, range: 1...3, do: {
            let newKey = Keys.baseKey + String(key)
            UserDefaults.standard.setValue(value, forKey: newKey)
//            print("newValue: \(value), forKey: \(newKey)")
        })
    }
    
    static private func inRange(value: Int, range: ClosedRange<Int>) -> Bool {
        range ~= value
    }
    
    static private func inRange(value: Int, range: ClosedRange<Int>, do action: () -> Void) {
        if range ~= value {
            action()
        }
    }
    
    static func setApplicationDefault() {
        SettingsBundle.showPhase1 = true
        SettingsBundle.showPhase2 = true
        SettingsBundle.showPhase3 = true
        SettingsBundle.didAlreadyLaunch = true
    }
}
