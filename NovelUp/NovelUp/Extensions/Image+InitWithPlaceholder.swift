//
//  Image.swift
//  NovelUp
//
//  Created by Rodrigo Carvalho on 23/11/20.
//

import SwiftUI

extension Image {
    
    public init(data: Data?, placeholder: String) {
        guard let data = data,
              let uiImage = UIImage(data: data) else {
            self = Image(placeholder)
            return
        }
        self = Image(uiImage: uiImage)
    }
}
