//
//  Bool+NumericValue.swift
//  NovelUp
//
//  Created by Felipe Nishino on 25/11/20.
//

import Foundation

extension Bool {
    var numericValue : Int {
        if self {
            return 1
        }
        return 0
    }
}

func powInt (num: Int, power: Int) -> Int {
    var newValue : Int = 1
    if power != 0 {
        for _ in 1...power {
            newValue *= num
        }
    }
    return newValue
}

/// Opposed to the usual swift modulo behaviour in which -1 % 3 = -1, this operator implements the actual modulo, in which -1 % 3 = 2
infix operator %% : MultiplicationPrecedence
extension Int {
    static func %% (lhs: Int, rhs: Int) -> Int {
        let r = lhs % rhs
        return r < 0 ? r + rhs : r
    }
}
