//
//  NovelUpApp.swift
//  NovelUp
//
//  Created by Gustavo Rigor on 26/10/20.
//

import SwiftUI

@main
struct NovelUpApp: App {
    let persistenceController = PersistenceController.shared
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
