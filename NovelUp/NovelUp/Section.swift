//
//  Section.swift
//  NovelUp
//
//  Created by Felipe Nishino on 27/11/20.
//

import Foundation
enum Section : Int {
    case description = 0
    case write
    case read
    
    init(index: Int16) {
        switch index {
        case 0:
            self = SettingsBundle.showPhase1 ? .description : .write
        case 1:
            self = .write
        default:
            self = .read
        }
    }
    
    mutating func changeSection(prev: Bool, fase: Int, showDescription: Bool, visibilityControls: [String:Bool]) -> Bool {
        if (self.rawValue == 0 && prev && fase == 1) || (self.rawValue == 2 && !prev && fase == 3) {
            return false
        }
        var didChangePage = false
        
        let oldSection = self.rawValue
        let newSection = abs((self.rawValue + powInt(num: -1, power: prev.numericValue)) %% 3)
        
        didChangePage = (newSection == 0 && oldSection == 2) || (newSection == 2 && oldSection == 0)
        
        let visibilityControl : Bool = visibilityControls[String(fase + Int(didChangePage ? 1 : 0))] ?? true
        if !visibilityControl {
            didChangePage = (newSection == 0 && oldSection == 1) ? true : didChangePage
        }
        
        if oldSection == 0 && visibilityControl != showDescription {
            DispatchQueue.global(qos: .utility).async {
                UserDefaults.getOrSetValue(value: showDescription, forKey: fase)
            }
        }
        
        if fase == 1 && oldSection == 1 && newSection == 0 && !visibilityControl {
            return false
        }
        
        switch newSection {
        case 0:
            self = visibilityControl ? .description : (oldSection == 1 ? .read : .write)
        case 1:
            self = .write
        case 2:
            self = .read
        default:
            print("abacaxi")
        }
        return didChangePage
    }
}
